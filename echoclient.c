#include "csapp.h"
#include <string.h>
#include <unistd.h>

int main(int argc, char **argv)
{
  int clientfd, pivote;
  char *port,*name_file,*host,*bytes,*mem;
  char buf[MAXLINE];
  rio_t rio;
  size_t n;
  FILE *archivo;
  pivote =0 ;
  if (argc != 4) {
    fprintf(stderr, "usage: %s <host> <port> <name_file>\n", argv[0]);
    exit(0);
  }
  host = argv[1];
  port = argv[2];
  name_file = argv[3];
  strcpy(buf,name_file);
  clientfd = Open_clientfd(host, port);
  while (1){
    write(clientfd,buf,sizeof(buf));
    bzero((char *)&buf,sizeof(buf));
    if (pivote!=1){
      read(clientfd,buf,sizeof(buf));
    } else {
      mem = (char *)malloc((atoi(bytes)+1)*sizeof(char));
      read(clientfd,mem,atoi(bytes)+1);
    }
    // leyendo el archivo
    if(pivote == 0 && strcmp(buf,"E")==0){
      printf("El archivo no pudo ser transferido\n");
      strcpy(buf,"Cancelar");
      pivote = -1;
      break;
    }
    switch(pivote){
      case 0:
        bytes = (char *)malloc(sizeof(buf));
        strcpy(bytes,buf);
        strcpy(buf,"Correcto");
        pivote++;
        break;
      case 1:
        /* Leyendo y enviando el archivo */
        archivo = fopen(name_file, "wb");
        fwrite(mem,1,atoi(bytes),archivo);
        fclose(archivo);
        /* Termina de recibirlo */
        printf("Exito! transferido archivo\n");
        strcpy(buf,"done");
        pivote++;
        break;
      default:
        break;
    }
  }
  Close(clientfd);
  exit(0);
}
