#include "csapp.h"
#include <sys/stat.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
void echo(int connfd);
int main(int argc, char **argv)
{
  int listenfd, connfd, pivote, size_read;
  unsigned int clientlen;
  struct sockaddr_in clientaddr;
  struct hostent *hp;
  char *haddrp, *port,*mem,*name_file,*bytes,buf[MAXLINE];
  rio_t rio;
  FILE *archivo;
  long filelen;
  size_t n;
  struct stat estado_archivo;
  pivote = 0;
  if (argc != 2) {
    fprintf(stderr, "usage: %s <port>\n", argv[0]);
    exit(0);
  }
  port = argv[1];
  listenfd = Open_listenfd(port);
  while (1) {
    clientlen = sizeof(clientaddr);
    connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);
    /* Determine the domain name and IP address of the client */
    hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,
        sizeof(clientaddr.sin_addr.s_addr), AF_INET);
    haddrp = inet_ntoa(clientaddr.sin_addr);
    printf("server connected to %s (%s)\n", hp->h_name, haddrp);
    Rio_readinitb(&rio, connfd);
    while (1) {
      read(connfd,buf,sizeof(buf));
      if (pivote == 0 && stat(buf, &estado_archivo)<0){
        printf("Archivo no encontrado\n");
        strcpy(buf,"E");
        break;
      }else{
        switch(pivote){
          case 0:
            printf("Archivo encontrado\n");
            name_file = (char *)malloc(sizeof(buf));
            strcpy(name_file,buf);
            printf("El archivo fue:%s\n",name_file);
            sprintf(buf,"%d", estado_archivo.st_size);
            bytes = (char *)malloc(sizeof(buf));
            strcpy(bytes,buf);
            break;
          case 1:
            printf("Cliente: %s\n",buf);
            /*  Lectura de archivo */
            archivo = fopen(name_file, "rb");
            mem = (char *)malloc((atoi(bytes)+1)*sizeof(char));
            fread(mem,atoi(bytes)+1,1,archivo);
            fclose(archivo);
            /*  finaliza lectura de archivo */
            write(connfd,mem,atoi(bytes)+1);
            pivote++;
            break;
          default:
            break;
        }
      }
    }
    printf("Cerrando\n");

    Close(connfd);
  }
  exit(0);
}
void echo(int connfd) {
}

